# dataHome directory

This Go module provides functionality to ensure the existence of a directory named `s51ds` inside the `user's home directory`.
If the directory does not already exist, it is automatically created.

## Usage

The only function in this module is `Dir()`, which returns the path to the `s51ds` directory within the `user's home directory`.

### Import the module in your Go code:

To use this module in your project, you need to get the module using `go get`.

```
go get gitlab.com/s51ds/datahome
```
### Example

Here is an example of how to use the `Dir()` function:

```
package main

import (
	"fmt"

	"gitlab.com/s51ds/datahome/dir"
)

func main() {
	path := s51ds.Dir()
	fmt.Println("Directory path:", path)
}
```