package s51ds

import (
	"os"
	"path/filepath"
)

// init setting up the "s51ds" directory in the user's home directory.
// It panics if there is an error during the process of retrieving the home directory,
// checking the existence of the "s51ds" directory, or creating it if it does not exist.
func init() {
	x, err := os.UserHomeDir()
	if err != nil {
		panic(err)
	}

	dir = filepath.Join(x, "s51ds")
	_, err = os.Stat(filepath.Join(x, "s51ds"))
	if os.IsNotExist(err) {
		err := os.MkdirAll(dir, os.ModePerm)
		if err != nil {
			panic(err)
		}
	}
}

var dir string

// Dir returns the path to the "s51ds" directory inside the user's home directory.
func Dir() string {
	return dir
}
