package s51ds

import (
	"os"
	"path/filepath"
	"testing"
)

func TestGet(t *testing.T) {
	x, _ := os.UserHomeDir()
	tests := []struct {
		name string
		want string
	}{
		{
			name: "1",
			want: filepath.Join(x, "s51ds"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := Dir()

			if got != tt.want {
				t.Errorf("Dir() got = %s, want %s", got, tt.want)
			}
		})
	}
}
